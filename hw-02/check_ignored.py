import argparse
import copy
import os

from re import match, compile, sub

parser = argparse.ArgumentParser(
    description='Check folder for gitignored files.')
parser.add_argument('-p', '--project-dir', dest='proj_dir', required=True,
                    help='Path to project directory')

args = parser.parse_args()

if __name__ == "__main__":
    args.proj_dir = args.proj_dir.rstrip()

    with open(os.path.join(args.proj_dir, '.gitignore')) as f:
        rules = f.readlines()

    rules_str = copy.copy(rules)  # make a copy of rules

    for i in range(len(rules)):
        rules[i] = compile(f"{args.proj_dir}{os.path.sep}{rules[i].replace('*', '.*')}")

    print("Ignored files:")

    for root, dirs, files in os.walk(args.proj_dir):
        for fname in files:
            for i, rule in enumerate(rules):
                if rule.match(os.path.join(root, fname)):
                    local_path = sub(f'^{args.proj_dir}?{os.path.sep}', '', root)
                    print(
                        f"{local_path}{os.path.sep}{fname}" + \
                        f" ignored by expression {rules_str[i]}")
