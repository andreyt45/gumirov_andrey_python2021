import argparse
import logging
import os
import sys
import time

parser = argparse.ArgumentParser()

parser.add_argument('-p', '--trash-folder-path',
                    help='Путь до папки, из которой будем удалять файлики',
                    required=True)
parser.add_argument('-t', '--age-thr',
                    help='Время "устаревания" файла/папки в секундах, после которого нужно удалять',
                    type=int,
                    required=True)
parser.add_argument('-d', '--dry-run',
                    help='"сухой" запуск. Не удаляем ничего.',
                    action='store_true')
args = parser.parse_args()

file_handler = logging.FileHandler(filename='trash_cleaner.log')
stdout_handler = logging.StreamHandler(sys.stdout)
handlers = [file_handler, stdout_handler]

logging.basicConfig(handlers=handlers,
                    encoding='utf-8',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logging.debug('meow!')

if __name__ == "__main__":
    logging.info('Starting trash cleaner!')

    while True:
        logging.info("Checking trash!")
        time_freezed = time.time()

        for root, _, files in os.walk(args.trash_folder_path):
            logging.debug(f"{root}, {_}, {files}")
            for file in files:
                full_file_path = os.path.join(root, file)
                if (time_freezed - os.path.getmtime(full_file_path)) / 60 > args.age_thr:
                    logging.info(f"Old file: {full_file_path}")
                    if not args.dry_run:
                        logging.warning(f"Removing file: {full_file_path}")
                        os.remove(full_file_path)

        for root, dirs, _ in os.walk(args.trash_folder_path):
            for dir in dirs:
                full_dir_path = os.path.join(root, dir)
                if os.path.exists(full_dir_path) and len(os.listdir(full_dir_path)) == 0:
                    logging.info(f"Empty dir: {dir}")
                    if not args.dry_run:
                        logging.warning(f"Removing dir: {full_dir_path}")
                        os.rmdir(full_dir_path)
        time.sleep(1)
