from flask import Flask, jsonify, request, render_template
from pandas import read_csv
from numpy import array

app = Flask(__name__)

app.config['JSON_AS_ASCII'] = False

hw1_table = read_csv('data/hw-01.csv')
hw2_table = read_csv('data/hw-02.csv')

tables = {
    "hw-01": hw1_table,
    "hw-02": hw2_table
}


def score2mark(score):
    mark = 2
    if score >= 1:
        mark = 3
    elif score >= 30:
        mark = 4
    elif score >= 50:
        mark = 5
    return mark


@app.route('/names')
def names():
    return jsonify({"names": list(map(lambda a: a.split(' ')[1], hw1_table['ФИ']))})


@app.route('/<hw_name>/mean_score')
def mean_score(hw_name):
    hw = tables[hw_name]
    return jsonify({"mean_score": str(hw.iloc[:, -1].mean())})


@app.route('/<hw_name>/<int:group_id>/mean_score')
def mean_group_score(hw_name, group_id):
    hw = tables[hw_name]
    hw = hw[hw['Группа'] == int(group_id)]
    return jsonify({"mean_score": str(hw.iloc[:, -1].mean())})


@app.route('/mean_score')
def mean_score_query():
    hw_name = request.args['hw_name']
    group_id = request.args['group_id']
    hw = tables[hw_name]
    hw = hw[hw['Группа'] == int(group_id)]
    return jsonify({"mean_score": str(hw.iloc[:, -1].mean())})


@app.route('/mark')
def mark():
    _mark = ''
    if 'student_id' in request.args:
        student_id = int(request.args['student_id'])-1
        score = hw1_table.iloc[student_id].iloc[-1] + hw2_table.iloc[student_id].iloc[-1]
        _mark = str(score2mark(score))
    elif 'group_id' in request.args:
        group_id = int(request.args['group_id'])
        mark = array(list(map(
            score2mark,
            list(
                hw1_table[hw1_table['Группа'] == group_id].iloc[:, -1] + \
                hw2_table[hw2_table['Группа'] == group_id].iloc[:, -1]
            )
        )))
        _mark = str(mark.mean())
    return jsonify({"mark": _mark})


@app.route('/course_table')
def course_table():
    hw_name = request.args['hw_name']
    hw = tables[hw_name]
    if 'group_id' in request.args:
        hw = hw[hw['Группа'] == int(request.args['group_id'])]
    return render_template('table.html', cols=hw.columns, rows=hw)

